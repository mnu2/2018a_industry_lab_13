package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {

    private BankAccount bankAccount = new BankAccount();
    private List<Transaction> transactions = TransactionGenerator.readDataFile();
    private BlockingQueue<Transaction> queue = new ArrayBlockingQueue<Transaction>(10);

    private class Consumer implements Runnable {
        @Override
        public void run(){
            boolean shouldRun = true;
            // while there is something in the queue and I've nbot been interrupted
            while (queue.size() > 0 || shouldRun) {
                try {
                    Transaction transaction = queue.take();
                    switch (transaction._type) {
                        case Deposit:
                            bankAccount.deposit(transaction._amountInCents);
                            break;
                        case Withdraw:
                            bankAccount.withdraw(transaction._amountInCents);
                            break;
                    }
                } catch (InterruptedException e) {
                    shouldRun = false;
                }
            }
        }

    }


    Runnable producer = new Runnable() {
        @Override
        public void run() {
            for (Transaction transaction: transactions){
                try {
                    queue.put(transaction);
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    };


    private Thread producerThread = new Thread(producer);
    private Thread consumer1 = new Thread(new Consumer());
    private Thread consumer2 = new Thread(new Consumer());

    public void start() {

        producerThread.start();
        consumer1.start();
        consumer2.start();

        try {
            producerThread.join();

            consumer1.interrupt();
            consumer2.interrupt();

            consumer1.join();
            consumer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Print the final balance after applying all Transactions.
        System.out.println("Final balance: " + bankAccount.getFormattedBalance());
    }

    public static void main(String[] args) {
        ConcurrentBankingApp ex = new ConcurrentBankingApp();
        ex.start();
    }
}
