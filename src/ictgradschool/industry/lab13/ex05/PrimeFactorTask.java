package ictgradschool.industry.lab13.ex05;

import ictgradschool.industry.lab13.examples.example03.PrimeFactors;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorTask implements Runnable {

    private enum TaskState {Initialized, Completed, Aborted}
    long value;
    List<Long> list = new ArrayList<>();
    TaskState status;


    public PrimeFactorTask(long n){
        value = n;
        status = TaskState.Initialized;
    }

    public long n(){
        return value;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException{
        return list;
    }

    public TaskState getState(){
        return status;
    }

    public void run(){


        // For each potential factor i.
            for (long i = 2; i * i <= value; i++) {
                // If i is a factor of N, repeatedly divide it out.
                while (value % i == 0 && !Thread.currentThread().isInterrupted()) {
                    list.add(i);
                    value = value / i;
                }
                if(Thread.currentThread().isInterrupted()){
                    status = TaskState.Aborted;
                    break;
                }
            }

            // If biggest factor occurs only once, n > 1
            if (value > 1) {
                list.add(value);
            }

        if (!Thread.currentThread().isInterrupted()){
            status = TaskState.Completed;
        }
    }

}
