package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.List;

public class PrimeFactorProgram {


    public void start(){
        System.out.println("Please enter a value: ");
        long n = Long.parseLong(Keyboard.readInput());
        PrimeFactorTask prime = new PrimeFactorTask(n);
        Thread thread = new Thread(prime);
        thread.start();
        String input = Keyboard.readInput();
        if (input.equals("A")){
            thread.interrupt();
        }

        else {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.getStackTrace();
            }
        }


        List<Long> list = prime.getPrimeFactors();
        for (Long s: list){
            System.out.print(s + " ");
        }
        System.out.println();
        System.out.println(prime.status);

    }

    public static void main(String[] args) {
        PrimeFactorProgram p = new PrimeFactorProgram();
        p.start();
    }
}
