package ictgradschool.industry.lab13.ex03;


import ictgradschool.Keyboard;
import ictgradschool.industry.lab13.ex01.RunnableProgram;

import javax.xml.transform.Result;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;



/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    private int subRoutines = 0;

    public void setSubRoutines(int num) {
        subRoutines = num;
    }

    private class MyPiRunnable implements Runnable {

                public double result = 0;
                private final long iterations;

                public MyPiRunnable(long iterations)
                {this.iterations  = iterations;}

                public void run(){
                    ExerciseThreeSingleThreaded exerciseThreeSingleThreaded = new ExerciseThreeSingleThreaded();

                    result = exerciseThreeSingleThreaded.estimatePI(iterations);

        }

    }

    public void start(){
        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        long numSamples = Long.parseLong(Keyboard.readInput());

        setSubRoutines(Integer.parseInt(Keyboard.readInput()));

        System.out.println("Estimating PI...");
        long startTime = System.currentTimeMillis();


        // Do the estimation
        double estimatedPi = this.estimatePI(numSamples);

        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(estimatedPi - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");
    }
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) {


        long subSamples = numSamples/subRoutines;

        double result = 0;

        List<Thread> threads = new ArrayList<>();
        List<MyPiRunnable> runnables = new ArrayList<>();

        for (int i = 0; i < subRoutines; i++){
            MyPiRunnable myPiRunnable = new MyPiRunnable(subSamples);
            Thread thread = new Thread(myPiRunnable);
            runnables.add(myPiRunnable);
            threads.add(thread);
            thread.start();
        }

        for (int j = 0; j < threads.size(); j++){

           try {
               threads.get(j).join();
               result += runnables.get(j).result;
           }
           catch (InterruptedException e){
               e.getStackTrace();
           }

        }

        return result/subRoutines;
    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
