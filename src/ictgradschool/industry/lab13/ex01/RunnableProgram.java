package ictgradschool.industry.lab13.ex01;

import javax.lang.model.element.VariableElement;

public class RunnableProgram {


    Thread runRunnable = new Thread(new Runnable() {
        @Override
        public void run() {
            int k = (int) (10000*Math.random());
            for (int i = 1; i < 1000001; i++) {
                if (i < k){
                System.out.println(i);
                    try {
                        runRunnable.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    System.out.println(i + ", " + k);

                }
            }
        }
    });

    public void start() {
        runRunnable.start();
    }

    public static void main(String[] args) {
        RunnableProgram ex = new RunnableProgram();
        ex.start();
    }
}
